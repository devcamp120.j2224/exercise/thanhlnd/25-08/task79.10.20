package com.devcamp.pizza365.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.pizza365.entity.*;

@Repository
public interface Customer2Repository extends JpaRepository<Customer2, Long> {

}
